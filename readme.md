# Example API and WEB

> https://franferri.io/templates

* Clone to get with: `git clone --depth 1 --recurse-submodules --shallow-submodules <repository-url>`
* Compile and run with: `./bin/dev_run.sh`
* Compile and run FAST with: `./bin/dev_run.sh --fast`
* Run tests: `./bin/tests_run.sh`
* Coverage: `./bin/report_conventions.sh`
* Conventions: `./bin/report_coverage.sh`
