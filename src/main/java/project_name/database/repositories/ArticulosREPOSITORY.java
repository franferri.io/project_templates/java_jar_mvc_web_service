package project_name.database.repositories;


import project_name.database.model.ArticuloMO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticulosREPOSITORY extends JpaRepository<ArticuloMO, Integer> {

    @Query(value = "SELECT * FROM ARTICULOS WHERE nombre=:name", nativeQuery = true)
    ArticuloMO getArticulo(String name);

}
